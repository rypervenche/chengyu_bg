use anyhow::Result;
use dotenvy_macro::dotenv;
use rusqlite::{self, params, Connection, Transaction};

const DB_FILE: &str = include_str!("../../resources/list");

fn create_db(conn: &mut Connection) -> rusqlite::Result<()> {
    let tx = conn.transaction()?;
    tx.execute(
        "CREATE TABLE IF NOT EXISTS chengyu (
            number INTEGER PRIMARY KEY AUTOINCREMENT,
            chengyu TEXT NOT NULL UNIQUE,
            zhuyin TEXT NOT NULL,
            definition TEXT NOT NULL,
            example1 TEXT NOT NULL,
            example2 TEXT NOT NULL,
            date_used TEXT UNIQUE
        )",
        params![],
    )?;

    tx.commit()
}
fn populate_db(tx: &Transaction, rows: &[String]) -> rusqlite::Result<()> {
    tx.execute(
        "INSERT INTO chengyu (
            chengyu,
            zhuyin,
            definition,
            example1,
            example2
        ) VALUES (
            ?1, ?2, ?3, ?4, ?5)",
        params![&rows[0], &rows[1], &rows[2], &rows[3], &rows[4]],
    )?;
    Ok(())
}

fn main() -> Result<()> {
    let mut conn = Connection::open(dotenv!("CHENGYU_DB"))?;
    create_db(&mut conn)?;

    let tx = conn.transaction()?;
    for (i, line) in DB_FILE.lines().enumerate() {
        if i == 0 {
            continue;
        }
        let rows: Vec<String> = line.split('|').map(|x| x.to_string()).collect();
        populate_db(&tx, &rows)?;
    }
    tx.commit()?;

    Ok(())
}
