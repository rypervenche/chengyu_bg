// TODO: Move createdb into its own lib
// TODO: Create database if it doesn't exist
// TODO: Add option to update existing database
use anyhow::Result;
use chengyu_bg::*;
use clap::Parser;
use dotenvy_macro::dotenv;
use rusqlite::Connection;

fn main() -> Result<()> {
    let opts = Opts::parse();
    let db = dotenv!("CHENGYU_DB");
    let conn = Connection::open(db)?;
    let chengyu = get_chengyu(&conn, &opts)?;
    create_png(&chengyu, &opts)?;
    set_wallpaper()?;
    Ok(())
}
