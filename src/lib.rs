//! Creates and sets background with daily Chengyu.

#![warn(missing_docs)]

use anyhow::Result;
use chrono::{Local, NaiveDate};
use clap::Parser;
use dirs::picture_dir;
use fontconfig::Fontconfig;
use lazy_static::lazy_static;
use rusqlite::{params, types::ValueRef, Connection};
use std::{path::PathBuf, process::Command};

lazy_static! {
    static ref FONT: String = {
        let fc = Fontconfig::new().unwrap();
        let font = fc.find("教育部標準楷書", Some("Regular")).unwrap();
        font.path.to_str().unwrap().to_string()
    };
    static ref OUTPUT_FILE: PathBuf = picture_dir().expect("Unable to find home directory.").join("current.png");
    // ${XDG_PICTURES_DIR}/current.png
}

/// Available Solarized Dark colors
#[allow(dead_code)]
enum Color {
    Base03,
    Base02,
    Base01,
    Base00,
    Base0,
    Base1,
    Base2,
    Base3,
    Yellow,
    Orange,
    Red,
    Magenta,
    Violet,
    Blue,
    Cyan,
    Green,
}

impl Color {
    fn hex(&self) -> &'static str {
        match self {
            Color::Base03 => "#002b36",
            Color::Base02 => "#073642",
            Color::Base01 => "#586e75",
            Color::Base00 => "#657b83",
            Color::Base0 => "#839496",
            Color::Base1 => "#93a1a1",
            Color::Base2 => "#eee8d5",
            Color::Base3 => "#fdf6e3",
            Color::Yellow => "#b58900",
            Color::Orange => "#cb4b16",
            Color::Red => "#dc322f",
            Color::Magenta => "#d33682",
            Color::Violet => "#6c71c4",
            Color::Blue => "#268bd2",
            Color::Cyan => "#2aa198",
            Color::Green => "#859900",
        }
    }
}

/// CLI Options
#[derive(Parser)]
#[clap(version, about = "Creates and sets background with daily Chengyu.")]
pub struct Opts {
    /// Choose a new Chengyu for today.
    #[clap(short, long)]
    pub new_chengyu: bool,

    /// Reset all Chengyu's set_used to NULL in database.
    #[clap(short, long)]
    pub reset_database: bool,

    /// Add zhuyin to wallpaper image.
    #[clap(short, long)]
    pub zhuyin: bool,
}

/// Chengyu from 學生成語詞典 book.
#[derive(Debug, Clone)]
pub struct Chengyu {
    /// Number of Chengyu in order of appearance in book.
    pub number: u32,

    /// The Chengyu in hanzi form.
    pub chengyu: String,

    /// The Taiwanese Mandarin pronunciation of the Chengyu written in zhuyin.
    pub zhuyin: String,

    /// Definition of the Chengyu.
    pub definition: String,

    /// First example of Chengyu usage.
    pub example1: String,

    /// Second example of Chengyu usage.
    pub example2: String,

    /// Whether Chengyu has been seen already or not.
    pub date_used: Option<NaiveDate>,
}

impl Chengyu {
    /// Create new Chengyu object.
    pub fn new(
        query: (u32, String, String, String, String, String, Option<&str>),
    ) -> Result<Chengyu> {
        let date = match query.6 {
            Some("") => None,
            Some(s) => Some(NaiveDate::parse_from_str(s, "%F")?),
            None => panic!("Bad input"),
        };
        Ok(Chengyu {
            number: query.0,
            chengyu: query.1,
            zhuyin: query.2,
            definition: query.3,
            example1: query.4,
            example2: query.5,
            date_used: date,
        })
    }
}

/// Run desired SQL query against database
fn run_sql_query(conn: &Connection, query_string: &str) -> rusqlite::Result<Result<Chengyu>> {
    let mut stmt = conn.prepare(query_string)?;

    stmt.query_row([], |row| {
        let date_used: &str = match row.get_ref_unwrap(6) {
            ValueRef::Null => "",
            ValueRef::Text(t) => std::str::from_utf8(t)?,
            _ => panic!("Bad 'used' value."),
        };

        Ok(Chengyu::new((
            row.get(0)?,
            row.get(1)?,
            row.get(2)?,
            row.get(3)?,
            row.get(4)?,
            row.get(5)?,
            Some(date_used),
        )))
    })
}

/// Get next Chengyu from database.
///
/// Grabs first unseen Chengyu from database and creates Chengyu object.
pub fn get_chengyu(conn: &Connection, opts: &Opts) -> Result<Chengyu> {
    let today = Local::now().naive_utc().date();

    if opts.reset_database {
        unset_all_chengyu(conn)?;
    } else if opts.new_chengyu {
        unset_today_chengyu(conn, today)?;
    }

    let result = run_sql_query(
        conn,
        &format!("SELECT * FROM chengyu WHERE date_used = \"{today}\" LIMIT 1;"),
    );

    let chengyu = match result {
        Ok(s) => s?,
        Err(rusqlite::Error::QueryReturnedNoRows) => {
            let new_chengyu = run_sql_query(
                conn,
                "SELECT * FROM chengyu WHERE date_used IS NULL ORDER BY RANDOM() LIMIT 1;",
            )??;

            set_date_used(conn, &new_chengyu.chengyu, today)?;
            new_chengyu
        }
        Err(_) => panic!("Something else bad happened."),
    };

    Ok(chengyu)
}

/// Set database Chengyu as used in database.
pub fn set_date_used(conn: &Connection, chengyu: &str, date: NaiveDate) -> Result<()> {
    conn.execute(
        "UPDATE chengyu SET date_used = ?1 WHERE chengyu = ?2",
        params![date.to_string(), chengyu],
    )?;
    Ok(())
}

/// Mark today's Chengyu as having not been used in database.
fn unset_today_chengyu(conn: &Connection, date: NaiveDate) -> Result<()> {
    conn.execute(
        "UPDATE chengyu SET date_used = NULL WHERE date_used = ?1",
        params![date.to_string()],
    )?;
    Ok(())
}

/// Mark all Chengyu as having not been used in database.
fn unset_all_chengyu(conn: &Connection) -> Result<()> {
    conn.execute(
        "UPDATE chengyu SET date_used = NULL WHERE date_used NOT NULL",
        params![],
    )?;
    Ok(())
}

/// Create wallpaper image.
///
/// Creates wallpaper image with a Chengyu and its definition.
pub fn create_png(chengyu: &Chengyu, opts: &Opts) -> Result<()> {
    let sentences = chengyu
        .definition
        .split_inclusive('。')
        .collect::<Vec<_>>()
        .join("\n");
    let caption1 = format!("caption:{}", chengyu.chengyu);
    let caption2 = if opts.zhuyin {
        format!("caption:({})\n\n{}", chengyu.zhuyin, &sentences)
    } else {
        format!("caption:{}", &sentences)
    };
    let caption3 = if chengyu.example1.is_empty() {
        "caption:".to_string()
    } else {
        format!(
            "caption:例一：{}\n例二：{}",
            chengyu.example1, chengyu.example2
        )
    };

    Command::new("convert")
        .arg("-size")
        .arg("1920x1056") // So i3bar doesn't cut off top
        .arg("-background")
        .arg(Color::Base03.hex())
        .arg("-fill")
        .arg(Color::Yellow.hex())
        .arg("-pointsize")
        .arg("300")
        .arg("-gravity")
        .arg("North")
        .arg("-font")
        .arg(&*FONT)
        .arg(caption1)
        .arg("-fill")
        .arg(Color::Base0.hex())
        .arg("-pointsize")
        .arg("100")
        .arg("-gravity")
        .arg("Center")
        .arg(caption2)
        .arg("-pointsize")
        .arg("60")
        .arg("-gravity")
        .arg("SouthWest")
        .arg(caption3)
        .arg("-transparent")
        .arg(Color::Base03.hex())
        .arg("-flatten")
        .arg(&*OUTPUT_FILE)
        .status()?;

    // This adds 24 pixels to top of image for i3bar
    Command::new("convert")
        .arg(&*OUTPUT_FILE)
        .arg("-background")
        .arg(Color::Base03.hex())
        .arg("-gravity")
        .arg("South")
        .arg("-extent")
        .arg("0x1080")
        .arg(&*OUTPUT_FILE)
        .status()?;
    Ok(())
}

/// Set wallpaper to current Chengyu wallpaper.
pub fn set_wallpaper() -> Result<()> {
    Command::new("feh")
        .arg("--bg-center")
        .arg(&*OUTPUT_FILE)
        .status()?;
    Ok(())
}
